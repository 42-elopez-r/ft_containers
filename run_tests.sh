#!/bin/sh

# Check for required commands
alias cmd_exists='command -v > /dev/null 2>&1'
if ! cmd_exists clang++ || ! cmd_exists make || ! cmd_exists bc; then
	echo "This utilities are required to run the tests:"
	echo "- clang"
	echo "- make"
	echo "- bc"
	echo "Install them and run the script again"
	exit 2
fi

# Make sure both tests are compiled
if ! make; then
	echo 'Build failed!'
	exit 1
fi

# Generate temp files for tests outputs
last_n="$(ls containers_ft_* 2> /dev/null | tail -n 1 | cut -d '_' -f 3)"
ft="containers_ft_$((last_n + 1))"
stl="containers_stl_$((last_n + 1))"

# Set UNIX time as common random seed for both tests
seed=$(date +%s)

# Run tests
./tests_ft --rnd-seed "$seed" --vector --map --stack > "$ft" || exit 1
./tests_stl --rnd-seed "$seed" --vector --map --stack > "$stl" || exit 1

# Show diff of outputs, piping to "bat" for colors if available
# (Macs implementation of diff doesn't support --color)
if cmd_exists bat; then
	diff "$stl" "$ft" | bat -l diff
else
	diff "$stl" "$ft"
fi

# Calculate times proportion between both test for each container
times_proportion()
{
	secs_stl="$(grep "Execution time of $1:" "$stl" |  cut -d ' ' -f 6)"
	usecs_stl="$(grep "Execution time of $1:" "$stl" |  cut -d ' ' -f 9)"
	secs_ft="$(grep "Execution time of $1:" "$ft" |  cut -d ' ' -f 6)"
	usecs_ft="$(grep "Execution time of $1:" "$ft" |  cut -d ' ' -f 9)"
	time_prop="$(bc << EOF
scale = 6
stl = $secs_stl + ($usecs_stl / 1000000)
ft = $secs_ft + ($usecs_ft / 1000000)
prop = ft / stl
scale = 3
prop
EOF
	)"
	printf "$1: FT version lasted %.3f times of the STL version\n" "$time_prop"
}
echo
times_proportion vector
#times_proportion map  # TODO
times_proportion stack

# Prompt for deleting or keeping the tests outputs
printf '\nRemove tests outputs? [Y/n]: '
read -r inp
if [ -z "$inp" ] || [ "$inp" = 'y' ] || [ "$inp" = 'Y' ]; then
	rm "$stl" "$ft" && echo 'Tests outputs removed'
else
	echo 'Tests outputs kept'
	echo
	echo "STL: $stl"
	echo "FT:  $ft"
fi
