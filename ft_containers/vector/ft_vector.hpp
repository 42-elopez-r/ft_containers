/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FT_VECTOR_HPP
#define FT_VECTOR_HPP

#include <cstddef>
#include <sstream>
#include <stdexcept>
#include <cstring>

#include "ft_vector_iterator.hpp"
#include "../iterators_common/ft_reverse_iterator.hpp"
#include "../iterators_common/ft_compare.hpp"

// Send help

namespace ft
{
	template < class T, class Allocator = std::allocator<T> >
	class vector
	{
		public:
			// =====
			// Types
			// =====
			typedef T										value_type;
			typedef Allocator								allocator_type;
			typedef std::size_t								size_type;
			typedef std::ptrdiff_t							difference_type;
			typedef typename Allocator::reference			reference;
			typedef typename Allocator::const_reference		const_reference;
			typedef typename Allocator::pointer				pointer;
			typedef typename Allocator::const_pointer		const_pointer;
			typedef vector_iterator<pointer>				iterator;
			typedef vector_iterator<const_pointer>			const_iterator;
			typedef ft::reverse_iterator<iterator>			reverse_iterator;
			typedef ft::reverse_iterator<const_iterator>	const_reverse_iterator;

			// ============
			// Constructors
			// ============

			/*
			 * Default constructor.
			 * Constructs an empty vector, with no elements.
			 */
			explicit vector(const allocator_type& alloc = allocator_type())
			{
				this->alloc = new allocator_type(alloc);
				this->elem = NULL;
				this->space = NULL;
				this->last = NULL;
			}

			/*
			 * Fill constructor.
			 * Constructs a container with n elements. Each element is a copy
			 * of val.
			 */
			explicit vector(size_type n, const value_type& val = value_type(),
					const allocator_type& alloc = allocator_type())
			{
				this->alloc = new allocator_type(alloc);

				this->elem = this->alloc->allocate(n);
				this->space = this->elem + n;
				this->last = this->space;

				for (size_type i = 0; i < n; i++)
					this->alloc->construct(this->elem + i, val);
			}

			/*
			 * Range constructor.
			 * Constructs a container with as many elements as the range
			 * [first,last), with each element constructed from its
			 * corresponding element in that range, in the same order.
			 */
			template <class InputIterator>
			vector(InputIterator first, InputIterator last,
					const allocator_type& alloc = allocator_type())
			{
				size_type len_used;

				this->alloc = new allocator_type(alloc);

				len_used = last - first;
				this->elem = this->alloc->allocate(len_used);
				this->space = this->elem + len_used;
				this->last = this->space;

				for (size_type i = 0; i < len_used; i++)
					this->alloc->construct(this->elem + i, *first++);
			}

			/*
			 * Copy constructor.
			 * Constructs a container with a copy of each of the elements in x,
			 * in the same order.
			 */
			vector(const vector<T, Allocator>& x)
			{
				size_type len;

				this->alloc = new allocator_type(*x.alloc);

				len = x.space - x.elem;
				this->elem = this->alloc->allocate(len);
				this->space = this->elem + len;
				this->last = this->elem + len;

				for (size_type i = 0; i < len; i++)
					this->alloc->construct(this->elem + i, x.elem[i]);
			}

			// ==========
			// Destructor
			// ==========
			~vector()
			{
				for (size_type i = 0; i < this->size(); i++)
					this->alloc->destroy(this->elem + i);

				this->alloc->deallocate(this->elem, this->capacity());
				delete this->alloc;
			}

			// =========
			// operator=
			// =========

			/*
			 * Assigns new contents to the container, replacing its current
			 * contents, and modifying its size accordingly.
			 */
			vector& operator= (const vector& x)
			{
				this->assign(x.begin(), x.end());
				return (*this);
			}

			// =========
			// Iterators
			// =========

			/*
			 * Returns an iterator pointing to the first element in the vector.
			 */
			iterator begin()
			{
				return (iterator(this->elem));
			}

			/*
			 * Returns a const_iterator pointing to the first element in the
			 * vector.
			 */
			const_iterator begin() const
			{
				return (const_iterator(this->elem));
			}

			/*
			 * Returns an iterator referring to the past-the-end element in
			 * the vector container.
			 */
			iterator end()
			{
				return (iterator(this->space));
			}

			/*
			 * Returns a const_iterator referring to the past-the-end element in
			 * the vector container.
			 */
			const_iterator end() const
			{
				return (const_iterator(this->space));
			}

			/*
			 * Returns a reverse_iterator pointing to the last element in the
			 * vector (i.e., its reverse beginning).
			 */
			reverse_iterator rbegin()
			{
				return reverse_iterator(this->end());
			}

			/*
			 * Returns a const_reverse_iterator pointing to the last element
			 * in the vector (i.e., its reverse beginning).
			 */
			const_reverse_iterator rbegin() const
			{
				return const_reverse_iterator(this->end());
			}

			/*
			 * Returns a reverse_iterator pointing to the last element in the
			 * vector (i.e., its reverse beginning).
			 */
			reverse_iterator rend()
			{
				return reverse_iterator(this->begin());
			}

			/*
			 * Returns a const_reverse_iterator pointing to the last element
			 * in the vector (i.e., its reverse beginning).
			 */
			const_reverse_iterator rend() const
			{
				return const_reverse_iterator(this->begin());
			}

			// ========
			// Capacity
			// ========

			/*
			 * Returns the number of elements in the vector.
			 */
			size_type size() const
			{
				return (this->space - this->elem);
			}

			/*
			 * Returns the maximum number of elements that the vector can hold.
			 */
			size_type max_size() const
			{
				return (this->alloc->max_size());
			}

			/*
			 * Returns the size of the storage space currently allocated for
			 * the vector, expressed in terms of elements.
			 */
			size_type capacity() const
			{
				return (this->last - this->elem);
			}

			/*
			 * Returns whether the vector is empty (i.e. whether its size is 0).
			 */
			bool empty() const
			{
				return (this->size() == 0);
			}

			/*
			 * Requests that the vector capacity be at least enough to contain
			 * n elements.
			 */
			void reserve(size_type n)
			{
				pointer new_elem;
				size_type prev_size;
				size_type prev_capacity;

				if (this->capacity() >= n)
					return;
				prev_capacity = this->capacity();
				prev_size = this->size();

				new_elem = this->alloc->allocate(n);
				for (size_type i = 0; i < prev_size; i++)
				{
					this->alloc->construct(new_elem + i, this->elem[i]);
					this->alloc->destroy(this->elem + i);
				}
				this->alloc->deallocate(this->elem, prev_capacity);

				this->elem = new_elem;
				this->space = this->elem + prev_size;
				this->last = this->elem + n;
			}

			/*
			 * Resizes the container so that it contains n elements.
			 */
			void resize(size_type n, value_type val = value_type())
			{
				if (n > this->size())
					this->insert(this->end(), n - this->size(), val);
				else if (n < this->size())
					this->erase(this->begin() + n, this->end());
			}

			// ==============
			// Element access
			// ==============

			/*
			 * Auxiliar methods
			 */
		private:
			void range_check(size_type n) const
			{
				std::stringstream ss;

				if (n < this->size())
					return;

				ss << "vector::_M_range_check: __n (which is " << n;
				ss << ") >= this->size() (which is " << this->size() << ")";
				throw std::out_of_range(ss.str());
			}
		public:

			/*
			 * Returns a reference to the element at position n in the vector
			 * container.
			 */
			reference operator[](size_type n)
			{
				return (this->elem[n]);
			}

			/*
			 * Returns a constant reference to the element at position n in the
			 * vector container.
			 */
			const_reference operator[](size_type n) const
			{
				return (this->elem[n]);
			}

			/*
			 * Returns a reference to the element at position n in the vector
			 * container.
			 */
			reference at(size_type n)
			{
				this->range_check(n);
				return (this->elem[n]);
			}

			/*
			 * Returns a constant reference to the element at position n in the
			 * vector container.
			 */
			const_reference at(size_type n) const
			{
				this->range_check(n);
				return (this->elem[n]);
			}

			/*
			 * Returns a reference to the first element in the vector.
			 */
			reference front()
			{
				return (*(this->elem));
			}

			/*
			 * Returns a constant reference to the first element in the vector.
			 */
			const_reference front() const
			{
				return (*(this->elem));
			}

			/*
			 * Returns a reference to the first element in the vector.
			 */
			reference back()
			{
				return (*(this->space - 1));
			}

			/*
			 * Returns a constant reference to the first element in the vector.
			 */
			const_reference back() const
			{
				return (*(this->space - 1));
			}

			// =========
			// Modifiers
			// =========

			/*
			 * The vector is extended by inserting new elements before the
			 * element at the specified position, effectively increasing the
			 * container size by the number of elements inserted.
			 */
			iterator insert(iterator position, const value_type& val)
			{
				size_type i_position;

				// Get index before reserve() invalidates the iterator
				i_position = position - this->begin();
				if (this->size() + 1 > this->capacity())
					this->reserve(this->size() ? this->size() * 2 : 1);

				memmove(this->elem + i_position + 1, this->elem + i_position,
						(this->size() - i_position) * sizeof(value_type));
				this->alloc->construct(this->elem + i_position, val);
				this->space++;

				return (iterator(this->elem + i_position));
			}

			/*
			 * The vector is extended by inserting new elements before the
			 * element at the specified position, effectively increasing the
			 * container size by the number of elements inserted.
			 */
			void insert(iterator position, size_type n, const value_type& val)
			{
				size_type i_position;

				// Get index before reserve() invalidates the iterator
				i_position = position - this->begin();
				if (this->size() + n > this->capacity())
					this->reserve(this->size() * 2 >= this->size() + n ?
							this->size() * 2 : this->size() + n);

				memmove(this->elem + i_position + n, this->elem + i_position,
						(this->size() - i_position) * sizeof(value_type));
				for (size_type i = 0; i < n; i++)
					this->alloc->construct(this->elem + i_position + i, val);
				this->space += n;
			}

			/*
			 * The vector is extended by inserting new elements before the
			 * element at the specified position, effectively increasing the
			 * container size by the number of elements inserted.
			 */
			template <class InputIterator>
			void insert(iterator position, InputIterator first,
					InputIterator last)
			{
				size_type n;
				size_type i_position;

				// Calculate ammount of new elements
				n = last - first;
				// Get index before reserve() invalidates the iterator
				i_position = position - this->begin();
				if (this->size() + n > this->capacity())
					this->reserve(this->size() * 2 >= this->size() + n ?
							this->size() * 2 : this->size() + n);

				memmove(this->elem + i_position + n, this->elem + i_position,
						(this->size() - i_position) * sizeof(value_type));
				for (size_type i = 0; i < n; i++)
					this->alloc->construct(this->elem + i_position + i, *first++);
				this->space += n;
			}

			/*
			 * Removes from the vector a single element.
			 */
			iterator erase(iterator position)
			{
				size_type i_position;

				i_position = position - this->begin();
				this->alloc->destroy(this->elem + i_position);
				memmove(this->elem + i_position, this->elem + i_position + 1,
						(this->size() - i_position - 1) * sizeof(value_type));
				this->space--;

				return (iterator(this->elem + i_position));
			}

			/*
			 * Removes from the vector a range of elements.
			 */
			iterator erase(iterator first, iterator last)
			{
				size_type n;
				size_type i_position;

				n = last - first;
				i_position = first - this->begin();
				for (size_type i = 0; i < n; i++)
					this->alloc->destroy(this->elem + i_position + i);
				memmove(this->elem + i_position, this->elem + i_position + n,
						(this->size() - i_position - n) * sizeof(value_type));
				this->space -= n;

				return (iterator(this->elem + i_position));
			}

			/*
			 * Removes all elements from the vector (which are destroyed),
			 * leaving the container with a size of 0.
			 */
			void clear()
			{
				this->erase(this->begin(), this->end());
			}

			/*
			 * Assigns new contents to the vector, replacing its current
			 * contents, and modifying its size accordingly.
			 *
			 * The new contents are elements constructed from each of the
			 * elements in the range between first and last, in the same order.
			 */
			template <class InputIterator>
			void assign(InputIterator first, InputIterator last)
			{
				this->clear();
				this->insert(this->begin(), first, last);
			}

			/*
			 * Assigns new contents to the vector, replacing its current
			 * contents, and modifying its size accordingly.
			 *
			 * The new contents are n elements, each initialized to a copy
			 * of val.
			 */
			void assign(size_type n, const value_type& val)
			{
				this->clear();
				this->insert(this->begin(), n, val);
			}

			/*
			 * Adds a new element at the end of the vector, after its current
			 * last element. The content of val is copied (or moved) to the
			 * new element.
			 */
			void push_back(const value_type& val)
			{
				this->insert(this->end(), val);
			}

			/*
			 * Removes the last element in the vector, effectively reducing
			 * the container size by one.
			 */
			void pop_back()
			{
				this->erase(this->end() - 1);
			}

			/*
			 * Exchanges the content of the container by the content of x,
			 * which is another vector object of the same type.
			 * Sizes may differ.
			 */
			void swap(vector& x)
			{
				pointer aux_elem;
				pointer aux_space;
				pointer aux_last;

				aux_elem = this->elem;
				aux_space = this->space;
				aux_last = this->last;
				this->elem = x.elem;
				this->space = x.space;
				this->last = x.last;
				x.elem = aux_elem;
				x.space = aux_space;
				x.last = aux_last;
			}

			// =========
			// Allocator
			// =========

			/*
			 *  Returns a copy of the allocator object associated with
			 *  the vector.
			 */
			allocator_type get_allocator() const
			{
				return (*(this->alloc));
			}
		private:
			allocator_type* alloc;
			pointer elem;
			pointer space;
			pointer last;
	};

	/*
	 * The comparison is performed by first comparing sizes, and if they match,
	 * the elements are compared sequentially using operator==,
	 * stopping at the first mismatch.
	 */
	template <class T, class Alloc>
	bool operator==(const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		if (lhs.size() != rhs.size())
			return (false);

		return (ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	/*
	 * Equivalent to !(lhs == rhs).
	 */
	template <class T, class Alloc>
	bool operator!=(const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (!(lhs == rhs));
	}

	/*
	 * Compares the elements sequentially using operator< in a reciprocal
	 * manner (i.e., checking both a<b and b<a) and stopping at the
	 * first occurrence.
	 */
	template <class T, class Alloc>
	bool operator<(const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(),
					rhs.begin(), rhs.end()));
	}

	/*
	 * Equivalent to !(rhs < lhs).
	 */
	template <class T, class Alloc>
	bool operator<=(const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (!(rhs < lhs));
	}

	/*
	 * Equivalent to rhs < lhs.
	 */
	template <class T, class Alloc>
	bool operator>(const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (rhs < lhs);
	}

	/*
	 * Equivalent to !(lhs < rhs).
	 */
	template <class T, class Alloc>
	bool operator>=(const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (!(lhs < rhs));
	}

	/*
	 * The contents of container x are exchanged with those of y.
	 * Both container objects must be of the same type (same template
	 * parameters), although sizes may differ.
	 */
	template <class T, class Alloc>
	void swap(vector<T,Alloc>& x, vector<T,Alloc>& y)
	{
		x.swap(y);
	}
}

#endif
