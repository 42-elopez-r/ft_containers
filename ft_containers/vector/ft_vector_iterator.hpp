/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FT_VECTOR_ITERATOR_HPP
#define FT_VECTOR_ITERATOR_HPP

#include "../iterators_common/ft_iterator_traits.hpp"

namespace ft
{
	template <class P>
	class vector_iterator
	{
		public:
			// =====
			// Types
			// =====

			typedef typename iterator_traits<P>::value_type			value_type;
			typedef typename iterator_traits<P>::difference_type	difference_type;
			typedef typename iterator_traits<P>::pointer			pointer;
			typedef typename iterator_traits<P>::reference			reference;
			typedef typename iterator_traits<P>::iterator_category	iterator_category;

			// ============
			// Constructors
			// ============

			/*
			 * Default constructor.
			 * Constructs a vector_iterator pointing to the passed element, or
			 * to NULL by default.
			 */
			vector_iterator(pointer elem = NULL)
			{
				this->elem = elem;
			}

			/*
			 * Copy constructor.
			 * Constructs a vector_iterator pointing to the same element as the
			 * passed iterator.
			 */
			template <class X>
			vector_iterator(const vector_iterator<X>& v_i)
			{
				this->elem = v_i.get_elem();
			}

			// =========
			// Operators
			// =========

			/*
			 * Assignment operator.
			 */
			template <class X>
			vector_iterator& operator=(const vector_iterator<X>& v_i)
			{
				this->elem = v_i.get_elem();

				return (*this);
			}

			/*
			 * Equality operator.
			 */
			template <class X>
			bool operator==(const vector_iterator<X>& v_i) const
			{
				return (this->elem == v_i.get_elem());
			}

			/*
			 * Inequality operator.
			 */
			template <class X>
			bool operator!=(const vector_iterator<X>& v_i) const
			{
				return (this->elem != v_i.get_elem());
			}

			/*
			 * Dereferenciation operator.
			 */
			reference operator*()
			{
				return (*(this->elem));
			}

			/*
			 * Dereferenciation arrow operator.
			 */
			pointer operator->()
			{
				return (this->elem);
			}

			/*
			 * Pre-increment operator.
			 */
			vector_iterator& operator++()
			{
				this->elem++;

				return(*this);
			}

			/*
			 * Post-increment operator.
			 */
			vector_iterator operator++(int)
			{
				vector_iterator temp(*this);

				this->elem++;
				return(temp);
			}

			/*
			 * Pre-decrement operator.
			 */
			vector_iterator& operator--()
			{
				this->elem--;

				return(*this);
			}

			/*
			 * Post-decrement operator.
			 */
			vector_iterator operator--(int)
			{
				vector_iterator temp(*this);

				this->elem--;
				return(temp);
			}

			/*
			 * Arithmethic addition operator, iterator first.
			 */
			vector_iterator operator+(difference_type n) const
			{
				return (vector_iterator(this->elem + n));
			}

			/*
			 * Arithmethic substraction operator.
			 */
			vector_iterator operator-(difference_type n) const
			{
				return (vector_iterator(this->elem - n));
			}

			/*
			 * Iterator substraction operator.
			 */
			template <class X>
			difference_type operator-(const vector_iterator<X>& v_i) const
			{
				return (this->elem - v_i.get_elem());
			}

			/*
			 * Less than operator.
			 */
			template <class X>
			bool operator<(const vector_iterator<X>& v_i) const
			{
				return (this->elem < v_i.get_elem());
			}

			/*
			 * Bigger than operator.
			 */
			template <class X>
			bool operator>(const vector_iterator<X>& v_i) const
			{
				return (this->elem > v_i.get_elem());
			}

			/*
			 * Less or equal operator.
			 */
			template <class X>
			bool operator<=(const vector_iterator<X>& v_i) const
			{
				return (this->elem <= v_i.get_elem());
			}

			/*
			 * Bigger or equal operator.
			 */
			template <class X>
			bool operator>=(const vector_iterator<X>& v_i) const
			{
				return (this->elem >= v_i.get_elem());
			}

			/*
			 * Compound assignment operator.
			 */
			vector_iterator& operator+=(difference_type n)
			{
				this->elem += n;
				return (*this);
			}

			/*
			 * Compound substraction operator.
			 */
			vector_iterator& operator-=(difference_type n)
			{
				this->elem -= n;
				return (*this);
			}

			/*
			 * Offset dereference operator.
			 */
			reference operator[](difference_type n)
			{
				return (this->elem[n]);
			}

			/*
			 * Offset const dereference operator.
			 */
			const reference operator[](difference_type n) const
			{
				return (this->elem[n]);
			}

			/*
			 * Returns the inner pointer being used by the iterator.
			 * Useful to interface vector_iterators of different kind (normal,
			 * consts)
			 */
			pointer get_elem() const
			{
				return (elem);
			}

		private:
			pointer elem;
	};

	/*
	 * Arithmethic addition operator, integer first.
	 */
	template < class P >
	vector_iterator<P> operator+(typename vector_iterator<P>::difference_type n,
			const vector_iterator<P>& v_i)
	{
		return (v_i + n);
	}
}

#endif
