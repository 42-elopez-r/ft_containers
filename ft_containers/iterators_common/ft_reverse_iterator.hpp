/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FT_REVERSE_ITERATOR_HPP
#define FT_REVERSE_ITERATOR_HPP

#include "ft_iterator_traits.hpp"

namespace ft
{
	template <class Iterator>
	class reverse_iterator
	{
		public:
			// =====
			// Types
			// =====
			
			typedef Iterator												iterator_type;
			typedef typename iterator_traits<Iterator>::difference_type		difference_type;
			typedef typename iterator_traits<Iterator>::value_type			value_type;
			typedef typename iterator_traits<Iterator>::pointer				pointer;
			typedef typename iterator_traits<Iterator>::reference			reference;
			typedef typename iterator_traits<Iterator>::iterator_category	iterator_category;

			// ============
			// Constructors
			// ============

			/*
			 * Default constructor.
			 */
			reverse_iterator() : cur_it()
			{}

			/*
			 * Initialization constructor that takes an original iterator
			 * to build the reverse_iterator.
			 */
			explicit reverse_iterator(iterator_type it) : cur_it(it - 1)
			{}

			/*
			 * Copy constructor.
			 */
			reverse_iterator(const reverse_iterator& rev_it) : cur_it(rev_it.cur_it)
			{}

			// =====================
			// Methods and operators
			// =====================

			/*
			 * Returns a copy of the base iterator.
			 */
			iterator_type base() const
			{
				return (this->cur_it + 1);
			}

			/*
			 * Returns a reference to the element pointed to by the iterator.
			 */
			reference operator*() const
			{
				return (*(this->cur_it));
			}

			/*
			 * Returns a reverse iterator pointing to the element located
			 * n positions away from the element the iterator currently
			 * points to.
			 */
			reverse_iterator operator+(difference_type n) const
			{
				return (reverse_iterator(this->base() - n));
			}

			/*
			 * Advances the reverse_iterator by one position.
			 */
			reverse_iterator& operator++()
			{
				--(this->cur_it);

				return (*this);
			}

			/*
			 * Advances the reverse_iterator by one position.
			 */
			reverse_iterator operator++(int)
			{
				reverse_iterator temp(*this);

				--(this->cur_it);
				return (temp);
			}

			/*
			 * Advances the reverse_iterator by n element positions.
			 */
			reverse_iterator& operator+= (difference_type n)
			{
				this->cur_it -= n;

				return (*this);
			}

			/*
			 *  Returns a reverse iterator pointing to the element located
			 *  n positions before the element the iterator currently points to.
			 */
			reverse_iterator operator-(difference_type n) const
			{
				return (reverse_iterator(this->base() + n));
			}

			/*
			 * Decreases the reverse_iterator by one position.
			 */
			reverse_iterator& operator--()
			{
				++(this->cur_it);

				return (*this);
			}

			/*
			 * Decreases the reverse_iterator by one position.
			 */
			reverse_iterator operator--(int)
			{
				reverse_iterator temp(*this);

				++(this->cur_it);
				return (temp);
			}

			/*
			 * Decreases the reverse_iterator by n element positions.
			 */
			reverse_iterator& operator-= (difference_type n)
			{
				this->cur_it += n;

				return (*this);
			}

			/*
			 * Returns a pointer to the element pointed to by the iterator
			 * (in order to access one of its members).
			 */
			pointer operator->() const
			{
				return &(*(this->cur_it));
			}

			/*
			 * Accesses the element located n positions away from the element
			 * currently pointed to by the iterator.
			 */
			reference operator[] (difference_type n) const
			{
				return (this->cur_it[-n]);
			}

		private:
			iterator_type cur_it;
	};

	/*
	 * Performs the equality comparison operation between the reverse_iterator
	 * objects lhs and rhs.
	 */
	template <class Iterator>
	bool operator==(const reverse_iterator<Iterator>& lhs,
			const reverse_iterator<Iterator>& rhs)
	{
		return (lhs.base() == rhs.base());
	}

	/*
	 * Performs the inequality comparison operation between the reverse_iterator
	 * objects lhs and rhs.
	 */
	template <class Iterator>
	bool operator!=(const reverse_iterator<Iterator>& lhs,
			const reverse_iterator<Iterator>& rhs)
	{
		return (lhs.base() != rhs.base());
	}

	/*
	 * Performs the bigger than comparison operation between the reverse_iterator
	 * objects lhs and rhs.
	 */
	template <class Iterator>
	bool operator>(const reverse_iterator<Iterator>& lhs,
			const reverse_iterator<Iterator>& rhs)
	{
		return (lhs.base() < rhs.base());
	}

	/*
	 * Performs the smaller than comparison operation between the reverse_iterator
	 * objects lhs and rhs.
	 */
	template <class Iterator>
	bool operator<(const reverse_iterator<Iterator>& lhs,
			const reverse_iterator<Iterator>& rhs)
	{
		return (lhs.base() > rhs.base());
	}

	/*
	 * Performs the bigger or equal comparison operation between the
	 * reverse_iterator objects lhs and rhs.
	 */
	template <class Iterator>
	bool operator>=(const reverse_iterator<Iterator>& lhs,
			const reverse_iterator<Iterator>& rhs)
	{
		return (lhs.base() <= rhs.base());
	}

	/*
	 * Performs the smaller or equal comparison operation between the
	 * reverse_iterator objects lhs and rhs.
	 */
	template <class Iterator>
	bool operator<=(const reverse_iterator<Iterator>& lhs,
			const reverse_iterator<Iterator>& rhs)
	{
		return (lhs.base() >= rhs.base());
	}

	/*
	 * Returns a reverse iterator pointing to the element located n positions
	 * away from the element pointed to by rev_it.
	 */
	template <class Iterator>
	reverse_iterator<Iterator> operator+(
			typename reverse_iterator<Iterator>::difference_type n,
			const reverse_iterator<Iterator>& rev_it)
	{
		return (rev_it + n);
	}

	/*
	 * Returns the distance between lhs and rhs.
	 */
	template <class Iterator>
	typename reverse_iterator<Iterator>::difference_type operator-(
			const reverse_iterator<Iterator>& lhs,
			const reverse_iterator<Iterator>& rhs)
	{
		return (rhs.base() - lhs.base());
	}
}

#endif
