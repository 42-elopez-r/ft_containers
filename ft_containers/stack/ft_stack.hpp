/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FT_STACK_HPP
#define FT_STACK_HPP

#include "../vector/ft_vector.hpp"

namespace ft
{
	template <class T, class Container = ft::vector<T> >
	class stack
	{
		public:
			// =====
			// Types
			// =====
			typedef T									value_type;
			typedef Container							container_type;
			typedef typename container_type::size_type	size_type;

			// ==================
			// Friend non-members
			// ==================

			/*
			 * Friend use justification:
			 * The relational operators of stack must call the ones belonging
			 * to the inner container, which is a private member and doesn't
			 * have a getter method.
			 * The GCC implementation of the STL uses friend too for them.
			 */
			template <class X, class Kontainer>
			friend bool operator==(const stack<X,Kontainer>& lhs, const stack<X,Kontainer>& rhs);
			template <class X, class Kontainer>
			friend bool operator!=(const stack<X,Kontainer>& lhs, const stack<X,Kontainer>& rhs);
			template <class X, class Kontainer>
			friend bool operator<(const stack<X,Kontainer>& lhs, const stack<X,Kontainer>& rhs);
			template <class X, class Kontainer>
			friend bool operator<=(const stack<X,Kontainer>& lhs, const stack<X,Kontainer>& rhs);
			template <class X, class Kontainer>
			friend bool operator>(const stack<X,Kontainer>& lhs, const stack<X,Kontainer>& rhs);
			template <class X, class Kontainer>
			friend bool operator>=(const stack<X,Kontainer>& lhs, const stack<X,Kontainer>& rhs);

			// ============
			// Constructors
			// ============

			/*
			 * Constructs a stack container adaptor object.
			 */
			explicit stack(const container_type& ctnr = container_type()):
				container(ctnr)
			{
			}

			// ========
			// Capacity
			// ========

			/*
			 * Returns whether the stack is empty.
			 */
			bool empty() const
			{
				return (this->container.empty());
			}

			/*
			 * Returns the number of elements in the stack.
			 */
			size_type size() const
			{
				return (this->container.size());
			}

			// ==============
			// Element access
			// ==============

			/*
			 * Returns a reference to the top element in the stack.
			 */
			value_type& top()
			{
				return (this->container.back());
			}

			/*
			 * Returns a reference to the top element in the stack.
			 */
			const value_type& top() const
			{
				return (this->container.back());
			}

			// =========
			// Modifiers
			// =========

			/*
			 * Inserts a new element at the top of the stack, above its
			 * current top element.
			 */
			void push(const value_type& val)
			{
				this->container.push_back(val);
			}

			/*
			 * Removes the element on top of the stack, effectively reducing
			 * its size by one.
			 */
			void pop()
			{
				this->container.pop_back();
			}
		private:
			container_type container;
	};

	/*
	 * Equivalent to operator== applied to the inner container.
	 */
	template <class T, class Container>
	bool operator==(const stack<T,Container>& lhs, const stack<T,Container>& rhs)
	{
		return (lhs.container == rhs.container);
	}

	/*
	 * Equivalent to operator!= applied to the inner container.
	 */
	template <class T, class Container>
	bool operator!=(const stack<T,Container>& lhs, const stack<T,Container>& rhs)
	{
		return (lhs.container != rhs.container);
	}

	/*
	 * Equivalent to operator< applied to the inner container.
	 */
	template <class T, class Container>
	bool operator<(const stack<T,Container>& lhs, const stack<T,Container>& rhs)
	{
		return (lhs.container < rhs.container);
	}

	/*
	 * Equivalent to operator<= applied to the inner container.
	 */
	template <class T, class Container>
	bool operator<=(const stack<T,Container>& lhs, const stack<T,Container>& rhs)
	{
		return (lhs.container <= rhs.container);
	}

	/*
	 * Equivalent to operator> applied to the inner container.
	 */
	template <class T, class Container>
	bool operator>(const stack<T,Container>& lhs, const stack<T,Container>& rhs)
	{
		return (lhs.container > rhs.container);
	}

	/*
	 * Equivalent to operator>= applied to the inner container.
	 */
	template <class T, class Container>
	bool operator>=(const stack<T,Container>& lhs, const stack<T,Container>& rhs)
	{
		return (lhs.container >= rhs.container);
	}
}

#endif
