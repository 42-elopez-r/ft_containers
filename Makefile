# = Main build variables ==============================================
CXX = clang++
SHELL = /bin/sh
CXXFLAGS = -Wall -Werror -Wextra -Wpedantic -pedantic-errors -std=c++98
# =====================================================================

# = Directories ========
SRC_DIR = tests/
OBJS_STL_DIR = objs/stl/
OBJS_FT_DIR = objs/ft/
# ======================

# = Sources and objects listings =======================
SRCS = $(shell ls $(SRC_DIR)*.cpp | cut -d '/' -f 2)
OBJS_STL = $(addprefix $(OBJS_STL_DIR), $(SRCS:.cpp=.o))
OBJS_FT = $(addprefix $(OBJS_FT_DIR), $(SRCS:.cpp=.o))
# ======================================================

all: tests_stl tests_ft

# = Targets for objects ======
$(OBJS_STL_DIR):
	mkdir -p $(OBJS_STL_DIR)

$(OBJS_FT_DIR):
	mkdir -p $(OBJS_FT_DIR)

$(OBJS_STL): | $(OBJS_STL_DIR)

$(OBJS_FT): | $(OBJS_FT_DIR)
# ============================

# = Main targets =======================
tests_stl: $(OBJS_STL)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJS_STL)

tests_ft: $(OBJS_FT)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJS_FT)
# ======================================

# = Targets for building sources ===========================
$(OBJS_STL_DIR)%.o: $(SRC_DIR)%.cpp
	$(CXX) $(CXXFLAGS) -D STL=1 -D NAMESPACE=std -c $< -o $@

$(OBJS_FT_DIR)%.o: $(SRC_DIR)%.cpp
	$(CXX) $(CXXFLAGS) -D STL=0 -D NAMESPACE=ft -c $< -o $@
# ==========================================================

# = Cleaning targets ==========
clean:
	rm -rfv objs

fclean: clean
	rm -fv tests_stl tests_ft

re: fclean all
# =============================

# = Debugging =======
debug: CXXFLAGS += -g
debug: fclean all
# ===================

.PHONY: all clean fclean re debug
