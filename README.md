# FT_Containers

I don't write READMEs for my 42 projects, mostly because the subject is usually enough explanation. But the flexibility to design and implement this project makes it more necessary.

The objective is to rewrite the **C++98** version of the following STL containers:
- Vector
- Stack
- Map

It also requires to write some tests for them and to execute a maximum of 20 times slower than their STL counterparts.

### Project structure

The project is structured as follows:

- **ft_containers**: this folder contains all the headers implementing the containers.
- **tests**: the code with the test program for both the STL and FT versions.
- *run_tests.sh*: script to automatize the build, launch and comparison of the tests.

### Running the tests

#### Dependencies
- A UNIX-like system
- The Clang compiler *(GCC should work as well but the subject mandates to use Clang)*
- make
- bc *(only for run_tests.sh)*

#### Tests
In order to build the tests just run `make` in the project root directory. This should leave you with two executables: `tests_ft` and `tests_stl`. Each one is the test program of the **tests** directory built either with the STL or FT implementation of the containers. The diff between them **(given the same random seed, more on that later)** should be the same except for the FT/STL titles and the times.

The test program accepts the following CLI arguments:
- --vector: run the tests for the vector container.
- --stack: run the tests for the stack container.
- --map: run the tests for the map container.
- --rnd-seed <seed>: this argument sets the random seed for the tests that use random numbers. This allows you to ensure that when running the tests, they will perform exactly the same tests. If you don't set this argument, the seed will be the UNIX epoch in seconds.

#### run_tests.sh
This is a convenience script that will automatically check for dependencies, build the tests, run them and show you the diffs of their outputs (storing them also in files for further analysis if desired) and calculate the proportion of time between the STL and FT versions. To use it, just execute it without CLI arguments and let the magic happen.
