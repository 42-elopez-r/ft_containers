/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstdlib>
#include "Potato.hpp"
#include "tests.hpp"

#if STL == 1
# include <vector>
#else
# include "../ft_containers/vector/ft_vector.hpp"
#endif

using std::cout; using std::endl;

/*
 * Displays the size and capacity of a vector to stdout.
 */
template <class T>
static void sizes(const NAMESPACE::vector<T>* v)
{
	cout << "size: " << v->size();
	cout << " capacity: " << v->capacity() << endl;
}

/*
 * Displays the elements of a vector using vector::at() to access them.
 */
template <class T>
static void display_vector(const NAMESPACE::vector<T>* v)
{
	try
	{
		for (size_t i = 0; i < v->size(); i++)
			cout << i << ": " << v->at(i) << endl;
	}
	catch (std::out_of_range& oor)
	{
		cout << oor.what() << endl;
	}
}

/*
 * Battery of tests for the constructors.
 */
static void test_constructors()
{

	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>* v_source;

	cout << "==> Vector constructors tests" << endl << endl;

	cout << "vector<Potato>() <- default constructor" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	sizes(v1);
	delete v1;

	cout << endl << "vector<Potato>(20, Potato(374)) <- fill constructor" << endl;
	v1 = new NAMESPACE::vector<Potato>(20, Potato(374));
	sizes(v1);
	display_vector(v1);
	delete v1;

	// Generare a vector to copy elements from
	v_source = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 5; i++)
		v_source->insert(v_source->end(), Potato(i));

	cout << endl << "vector<Potato>(first, last) <- range constructor" << endl;
	v1 = new NAMESPACE::vector<Potato>(v_source->begin(), v_source->end());
	sizes(v1);
	display_vector(v1);
	delete v1;

	cout << endl << "vector<Potato>(vector) <- copy constructor" << endl;
	v1 = new NAMESPACE::vector<Potato>(*v_source);
	sizes(v1);
	display_vector(v1);
	delete v1;

	delete v_source;
}

/*
 * Battery of tests for the operator= method.
 */
static void test_operator_assignation()
{
	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>* v_source;

	cout << endl << "==> Vector operator= tests" << endl << endl;

	// Generare a vector to copy elements from
	v_source = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 5; i++)
		v_source->insert(v_source->end(), Potato(i));

	cout << "vector_1 initial state:" << endl;
	v1 = new NAMESPACE::vector<Potato>(30);
	sizes(v1);
	display_vector(v1);

	cout << endl << "vector_1 state after calling vector_1 = vector_2" << endl;
	if (&(*v1 = *v_source) != v1)
		cout << "INCORRECT operator= returned object" << endl;
	sizes(v1);
	display_vector(v1);
	delete v1;

	delete v_source;
}

/*
 * Battery of tests for the capacity methods.
 */
static void test_capacity_methods()
{
	NAMESPACE::vector<Potato>* v1;
	size_t size_n = rand() % 150;

	cout << endl << "==> Vector capacity tests" << endl << endl;

	// Fill constructor
	cout << "vector<Potato>(" << size_n << ")" << endl;
	v1 = new NAMESPACE::vector<Potato>(size_n);

	// size()
	cout << endl << "size() == " << size_n << ": ";
	cout << ((v1->size() == size_n) ? "True" : "False") << endl;

	// capacity()
	cout << endl << "capacity() == " << size_n << ": ";
	cout << ((v1->capacity() == size_n) ? "True" : "False") << endl;
	delete v1;

	// Fill constructor
	cout << endl << "vector<Potato>(5, Potato(123))" << endl;
	v1 = new NAMESPACE::vector<Potato>(5, Potato(123));
	sizes(v1);

	// reserve()
	cout << endl << "reserve(10)" << endl;
	v1->reserve(10);
	sizes(v1);

	// resize() with bigger size
	cout << endl << "resize(15, Potato(987))" << endl;
	v1->resize(15, Potato(987));
	sizes(v1);

	// resize() with bigger size and default value
	cout << endl << "resize(20)" << endl;
	v1->resize(20);
	sizes(v1);

	// Display vector elements
	cout << endl << "Displaying vector elements" << endl;
	display_vector(v1);

	// resize() with smaller size
	cout << endl << "resize(8)" << endl;
	v1->resize(8);
	sizes(v1);

	// empty() on a non-empty vector
	cout << endl << "empty() == false: ";
	cout << (!v1->empty() ? "True" : "False") << endl;

	delete v1;

	// Default constructor
	cout << endl << "vector<Potato>()" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	sizes(v1);

	// empty() on a empty vector
	cout << endl << "empty() == true: ";
	cout << (v1->empty() ? "True" : "False") << endl;

	delete v1;
}

/*
 * Battery of tests for the element access methods.
 */
static void tests_element_access_methods()
{
	NAMESPACE::vector<Potato>* v1;
	size_t size_n = rand() % 150 + 1;

	cout << endl << "==> Vector element access tests" << endl << endl;

	cout << "vector<Potato>() with " << size_n << " elements" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	for (size_t i = 0; i < size_n; i++)
		v1->insert(v1->end(), Potato(i));

	cout << "Access all elements sequentially with operator[]" << endl;
	for (size_t i = 0; i < size_n; i++)
		cout << (*v1)[i] << endl;

	cout << endl << "Try to access out of range element " << size_n;
	cout << " using at()" << endl;
	try
	{
		cout << v1->at(size_n) << endl;
		cout << "If this line is printing at() failed" << endl;
	}
	catch (std::out_of_range& oor)
	{
		cout << oor.what() << endl;
	}

	cout << endl << "&(vector[0]) == &(front()): ";
	cout << ((&(v1->at(0)) == &(v1->front())) ? "True" : "False") << endl;

	cout << endl << "&(vector[size() - 1]) == &(back()): ";
	cout << ((&(v1->at(v1->size() - 1)) == &(v1->back())) ? "True" : "False") << endl;

	delete v1;
}

/*
 * Battery of tests for the insert() modifier method with a single element.
 */
static void test_insert_single_element()
{
	NAMESPACE::vector<Potato>* v1;

	cout << "Testing progressive calls to insert(end(), val)" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 150; i++)
	{
		sizes(v1);
		if (v1->insert(v1->end(), Potato(i)) != v1->end() - 1)
			cout << "INCORRECT insert() return iterator" << endl;
	}
	cout << endl << "Displaying vector elements" << endl;
	display_vector(v1);

	cout << endl << "Testing progressive calls to insert(begin(), val)" << endl;
	for (int i = 0; i < 150; i++)
	{
		sizes(v1);
		if (v1->insert(v1->begin(), Potato(i)) != v1->begin())
			cout << "INCORRECT insert() return iterator" << endl;
	}
	cout << endl << "Displaying vector elements" << endl;
	display_vector(v1);
	delete v1;
}

/*
 * Battery of tests for the insert() modifier method with n elements.
 */
static void test_insert_n_elements()
{
	NAMESPACE::vector<Potato>* v1;
	int n = rand() % 6;

	cout << endl << "Testing progressive calls to insert(end(), " << n << ", val)" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 150; i++)
	{
		sizes(v1);
		v1->insert(v1->end(), n, Potato(i));
	}
	cout << endl << "Displaying vector elements" << endl;
	display_vector(v1);

	cout << endl << "Testing progressive calls to insert(begin(), " << n << ", val)" << endl;
	for (int i = 0; i < 150; i++)
	{
		sizes(v1);
		v1->insert(v1->begin(), n, Potato(i));
	}
	cout << endl << "Displaying vector elements" << endl;
	display_vector(v1);
	delete v1;
}

/*
 * Battery of tests for the insert() modifier method copying from a range of
 * iterators.
 */
static void test_insert_iterators()
{
	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>* v_source;

	// Generare a vector to copy elements from
	v_source = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 5; i++)
		v_source->insert(v_source->end(), Potato(i));

	cout << endl << "Testing progressive calls to insert(end(), first, last)" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 150; i++)
	{
		sizes(v1);
		v1->insert(v1->end(), v_source->begin(), v_source->end());
	}
	cout << endl << "Displaying vector elements" << endl;
	display_vector(v1);

	cout << endl << "Testing progressive calls to insert(begin(), first, last)" << endl;
	for (int i = 0; i < 150; i++)
	{
		sizes(v1);
		v1->insert(v1->begin(), v_source->begin(), v_source->end());
	}
	cout << endl << "Displaying vector elements" << endl;
	display_vector(v1);
	delete v1;
	delete v_source;
}

/*
 * Battery of tests for the erase() modifier method with a single element.
 */
static void test_erase_single_element()
{
	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>::iterator it;
	int random_pos;

	v1 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 150; i++)
		v1->insert(v1->end(), Potato(i));

	cout << endl << "Tests for erase(position). Vector initial state:" << endl;
	sizes(v1);

	random_pos = rand() % v1->size();
	cout << endl << "Erasing position " << random_pos << endl;
	if (v1->erase(v1->begin() + random_pos) != v1->begin() + random_pos)
		cout << "INCORRECT erase() return iterator" << endl;
	sizes(v1);
	cout << "Displaying vector elements" << endl;
	display_vector(v1);

	cout << endl << "Erasing all vector elements one by one" << endl;
	while (v1->erase(v1->begin()) != v1->end());
	sizes(v1);
	delete v1;
}

/*
 * Battery of tests for the erase() modifier method with a range of
 * iterators.
 */
static void test_erase_iterators()
{
	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>::iterator it;
	int random_pos;
	int n;

	v1 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 150; i++)
		v1->insert(v1->end(), Potato(i));

	cout << endl << "Tests for erase(first, last). Vector initial state:" << endl;
	sizes(v1);

	random_pos = rand() % v1->size();
	n = rand() % (v1->size() - random_pos);
	cout << endl << "Erasing from position " << random_pos;
	cout << " to position " << random_pos + n  << " (excluded)" << endl;
	if (v1->erase(v1->begin() + random_pos, v1->begin() + random_pos + n)
			!= v1->begin() + random_pos)
		cout << "INCORRECT erase() return iterator" << endl;
	sizes(v1);
	cout << "Displaying vector elements" << endl;
	display_vector(v1);

	cout << endl << "Erasing all vector elements from begin() to end()" << endl;
	v1->erase(v1->begin(), v1->end());
	sizes(v1);
	delete v1;
}

/*
 * Battery of tests for the clear() method.
 */
static void test_clear()
{
	NAMESPACE::vector<Potato>* v1;

	v1 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 150; i++)
		v1->insert(v1->end(), Potato(i));

	cout << endl << "Tests for clear(). Vector initial state:" << endl;
	sizes(v1);

	v1->clear();
	cout << endl << "Vector state after calling clear()" << endl;
	sizes(v1);

	delete v1;
}

/*
 * Battery of tests for the assign() methods.
 */
static void test_assign()
{
	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>* v_source;

	// Generare a vector to copy elements from
	v_source = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < 5; i++)
		v_source->insert(v_source->end(), Potato(i));

	cout << endl << "Tests for assign(first, last). Vector initial state:" << endl;
	v1 = new NAMESPACE::vector<Potato>(30);
	sizes(v1);
	display_vector(v1);

	cout << endl << "Vector state after calling assign(first, last)" << endl;
	v1->assign(v_source->begin(), v_source->end());
	sizes(v1);
	display_vector(v1);
	delete v1;

	cout << endl << "Tests for assign(n, val). Vector initial state:" << endl;
	v1 = new NAMESPACE::vector<Potato>(30);
	sizes(v1);
	display_vector(v1);

	cout << endl << "Vector state after calling assign(7, Potato(12))" << endl;
	v1->assign(7, Potato(12));
	sizes(v1);
	display_vector(v1);
	delete v1;

	delete v_source;
}

/*
 * Battery of tests for the push_back() method.
 */
static void test_push_back()
{
	NAMESPACE::vector<Potato>* v1;

	cout << endl << "Tests for push_back(val). Vector initial state:" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	sizes(v1);

	cout << endl << "Testing progressive calls to push_back(val)" << endl;
	for (int i = 0; i < 150; i++)
		v1->push_back(Potato(i));
	sizes(v1);
	display_vector(v1);
	delete v1;
}

/*
 * Battery of tests for the pop_back() method.
 */
static void test_pop_back()
{
	NAMESPACE::vector<Potato>* v1;
	int n;

	cout << endl << "Tests for pop_back(). Vector initial state:" << endl;
	v1 = new NAMESPACE::vector<Potato>(150);
	sizes(v1);

	n = rand() % 150;
	cout << endl << "Testing " << n << " calls to pop_back()" << endl;
	for (int i = 0; i < n; i++)
		v1->pop_back();
	sizes(v1);
	delete v1;
}

/*
 * Battery of tests for the swap(x) method and the swap(x, y) non-member
 * overload.
 */
static void test_swap(bool call_member_method)
{
	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>* v2;
	NAMESPACE::vector<Potato>::iterator v1_begin;
	NAMESPACE::vector<Potato>::iterator v1_end;
	NAMESPACE::vector<Potato>::iterator v2_begin;
	NAMESPACE::vector<Potato>::iterator v2_end;
	int len_1 = (rand() % 150) + 1;
	int len_2 = (rand() % 150) + 1;

	if (call_member_method)
		cout << endl << "Tests for swap(x)." << endl;
	else
		cout << endl << "Tests for swap(x, y)." << endl;
	cout << "Vector 1 initial state:" << endl;
	v1 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < len_1; i++)
		v1->insert(v1->end(), Potato(i));
	v1_begin = v1->begin();
	v1_end = v1->end();
	sizes(v1);
	display_vector(v1);

	cout << endl << "Vector 2 initial state:" << endl;
	v2 = new NAMESPACE::vector<Potato>();
	for (int i = 0; i < len_2; i++)
		v2->insert(v2->end(), Potato(i));
	v2_begin = v2->begin();
	v2_end = v2->end();
	sizes(v2);
	display_vector(v2);

	if (call_member_method)
	{
		cout << endl << "Testing vector_1.swap(vector_2)" << endl;
		v1->swap(*v2);
	}
	else
	{
		cout << endl << "Testing swap(vector_1, vector_2)" << endl;
		swap(*v1, *v2);
	}

	// Sizes and content after swap
	cout << endl << "Vector 1 after swap state:" << endl;
	sizes(v1);
	display_vector(v1);

	cout << endl << "Vector 2 after swap state:" << endl;
	sizes(v2);
	display_vector(v2);

	// Verify iterators validity
	cout << endl << "previous_vector_1.begin() == vector_2.begin(): ";
	cout << ((v1_begin == v2->begin()) ? "True" : "False") << endl;
	cout << endl << "previous_vector_1.end() == vector_2.end(): ";
	cout << ((v1_end == v2->end()) ? "True" : "False") << endl;

	cout << endl << "previous_vector_2.begin() == vector_1.begin(): ";
	cout << ((v2_begin == v1->begin()) ? "True" : "False") << endl;
	cout << endl << "previous_vector_2.end() == vector_1.end(): ";
	cout << ((v2_end == v1->end()) ? "True" : "False") << endl;

	delete v1;
	delete v2;
}

/*
 * Battery of tests for the modifiers methods.
 */
static void test_modifiers_methods()
{
	cout << endl << "==> Vector modifiers tests" << endl << endl;

	test_insert_single_element();
	test_insert_n_elements();
	test_insert_iterators();
	test_erase_single_element();
	test_erase_iterators();
	test_clear();
	test_assign();
	test_push_back();
	test_pop_back();
	test_swap(true);
}

/*
 * Battery of tests for the allocator methods.
 */
static void test_allocator()
{
	NAMESPACE::vector<Potato>* v1;

	cout << endl << "==> Vector get_allocator() test" << endl << endl;

	v1 = new NAMESPACE::vector<Potato>();
	cout << "get_allocator().max_size() == max_size(): ";
	cout << ((v1->get_allocator().max_size()== v1->max_size()) ?
			"True" : "False") << endl;

	delete v1;
}

/*
 * Battery of tests for the non-member relational operators.
 */
static void test_relational_operators()
{
	NAMESPACE::vector<Potato>* v1;
	NAMESPACE::vector<Potato>* v2;

	v1 = new NAMESPACE::vector<Potato>(150);
	v2 = new NAMESPACE::vector<Potato>(*v1);

	cout << "Comparing two equal vectors" << endl;
	cout << "vector_1 == vector_2: ";
	cout << ((*v1 == *v2) ? "True" : "False") << endl;
	cout << "(vector_1 != vector_2) == False: ";
	cout << (!(*v1 != *v2) ? "True" : "False") << endl;

	v2->push_back(Potato(10));
	cout << endl << "Comparing two unequal vectors" << endl;
	cout << "(vector_1 == vector_2) == False: ";
	cout << (!(*v1 == *v2) ? "True" : "False") << endl;
	cout << "vector_1 != vector_2: ";
	cout << ((*v1 != *v2) ? "True" : "False") << endl;
	v2->pop_back();

	v2->insert(v2->begin() + 15, Potato(1000));
	cout << endl << "Given a bigger vector_2" << endl;
	cout << "vector_1 < vector_2: ";
	cout << ((*v1 < *v2) ? "True" : "False") << endl;
	cout << "(vector_1 > vector_2) == False: ";
	cout << (!(*v1 > *v2) ? "True" : "False") << endl;
	cout << "vector_1 <= vector_2: ";
	cout << ((*v1 <= *v2) ? "True" : "False") << endl;
	cout << "(vector_1 >= vector_2) == False: ";
	cout << (!(*v1 >= *v2) ? "True" : "False") << endl;

	delete v1;
	delete v2;
}

/*
 * Battery of tests for the non-members methods.
 */
static void test_non_members()
{
	cout << endl << "==> Vector non-members tests" << endl << endl;

	test_relational_operators();
	test_swap(false);
}

/*
 * Battery of tests for vector.
 */
void tests_vector()
{
#if STL == 1
	cout << "####################" << endl;
	cout << "# STL VECTOR TESTS #" << endl;
	cout << "####################" << endl << endl;
#else
	cout << "###################" << endl;
	cout << "# FT VECTOR TESTS #" << endl;
	cout << "###################" << endl << endl;
#endif

	test_constructors();
	test_operator_assignation();
	test_capacity_methods();
	tests_element_access_methods();
	tests_vector_iterators();
	tests_vector_reverse_iterator();
	test_modifiers_methods();
	test_allocator();
	test_non_members();
}
