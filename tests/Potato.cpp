/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Potato.hpp"

#include <iostream>

Potato::Potato()
{
	this->n = new int(42);
}

Potato::Potato(int n)
{
	this->n = new int(n);
}

Potato::Potato(const Potato& p)
{
	this->n = new int(p.get_n());
}

Potato& Potato::operator=(const Potato& p)
{
	this->set_n(p.get_n());

	return (*this);
}

Potato::~Potato()
{
	delete this->n;
}

int Potato::get_n() const
{
	return (*n);
}

void Potato::set_n(int n)
{
	*(this->n) = n;
}

bool Potato::operator==(const Potato& p) const
{
	return (this->get_n() == p.get_n());
}

bool Potato::operator<(const Potato& p) const
{
	return (this->get_n() < p.get_n());
}

ostream& operator<<(ostream& os, const Potato& p)
{
	os << p.get_n();
	return (os);
}
