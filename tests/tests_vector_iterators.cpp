/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "Potato.hpp"

#if STL == 1
# include <vector>
# include <iterator>
#else
# include "../ft_containers/vector/ft_vector.hpp"
# include "../ft_containers/iterators_common/ft_reverse_iterator.hpp"
#endif

using std::cout; using std::endl;
using std::ostream;

// Iterator types for simplicity
typedef NAMESPACE::vector<Potato>::iterator iterator_t;
typedef NAMESPACE::vector<Potato>::const_iterator const_iterator_t;
typedef NAMESPACE::reverse_iterator<iterator_t> rev_iterator_t;
typedef NAMESPACE::reverse_iterator<const_iterator_t> rev_const_iterator_t;

/*
 * Battery of tests for vector's iterators.
 */
void tests_vector_iterators()
{
	NAMESPACE::vector<Potato>* v1;
	iterator_t it;
	size_t i;

	cout << endl << "==> Vector iterator tests" << endl << endl;

	cout << "vector<Potato>(5)" << endl;
	v1 = new NAMESPACE::vector<Potato>(5);

	// Assign values to Potatoes in the vector
	cout << endl << "Iterate through the vector setting a value to each Potato";
	cout << " using a iterator (=, !=, it++, ->, begin(), end())" << endl;
	i = 0;
	for (it = v1->begin(); it != v1->end(); it++)
		it->set_n(i++);

	// Iterate through the vector
	cout << "Display all vector Potatoes incrementing a const_iterator";
	cout << " (copy constructor, !=, it++, *, begin(), end())" << endl;
	for (const_iterator_t c_it = v1->begin(); c_it != v1->end(); c_it++)
		cout << *c_it << endl;

	// Testing it+n, ==
	cout << endl << "it+n: (begin() + size()) == end(): ";
	cout << ((v1->begin() + v1->size()) == v1->end() ? "True" : "False") << endl;
	// Testing n+it, ==
	cout << "n+it: (size() + begin()) == end(): ";
	cout << ((v1->size() + v1->begin()) == v1->end() ? "True" : "False") << endl;

	// Testing it-n, ==
	cout << endl << "it-n: (end() - size()) == begin(): ";
	cout << ((v1->end() - v1->size()) == v1->begin() ? "True" : "False") << endl;
	// Testing it-it, ==
	cout << "it-it: (end() - begin()) == size(): ";
	cout << ((v1->end() - v1->begin()) == 
			static_cast<iterator_t::difference_type>(v1->size()) ?
			"True" : "False") << endl;

	// Testing it++, ==
	cout << endl << "it++: it = begin(); it++ == begin(): ";
	it = v1->begin();
	cout << ((it++ == v1->begin()) ? "True" : "False") << endl;

	// Testing ++it, ==, it+n
	cout << endl << "++it: it = begin(); ++it == (begin() + 1): ";
	it = v1->begin();
	cout << ((++it == (v1->begin() + 1)) ? "True" : "False") << endl;

	// Testing it--, ==
	cout << endl << "it--: it = begin(); it-- == begin(): ";
	it = v1->begin();
	cout << ((it-- == v1->begin()) ? "True" : "False") << endl;

	// Testing --it, ==, it-n
	cout << endl << "--it: it = begin(); --it == (begin() - 1): ";
	it = v1->begin();
	cout << ((--it == (v1->begin() - 1)) ? "True" : "False") << endl;

	// Testing it<it
	cout << endl << "it<it: begin() < end(): ";
	cout << ((v1->begin() < v1->end()) ? "True" : "False") << endl;

	// Testing it>it
	cout << endl << "it>it: end() > begin(): ";
	cout << ((v1->end() > v1->begin()) ? "True" : "False") << endl;

	// Testing it<=it
	cout << endl << "it<=it: begin() <= end(): ";
	cout << ((v1->begin() <= v1->end()) ? "True" : "False") << endl;
	cout << "it<=it: begin() <= begin(): ";
	cout << ((v1->begin() <= v1->begin()) ? "True" : "False") << endl;

	// Testing it>=it
	cout << endl << "it>=it: end() >= begin(): ";
	cout << ((v1->end() >= v1->begin()) ? "True" : "False") << endl;
	cout << "it>=it: begin() >= begin(): ";
	cout << ((v1->begin() >= v1->begin()) ? "True" : "False") << endl;

	// Testing =, +=, ==
	cout << endl << "it+=n: it = begin(); (it += size()) == end(): ";
	it = v1->begin();
	cout << (((it += v1->size()) == v1->end()) ? "True" : "False") << endl;

	// Testing =, -=, ==
	cout << endl << "it-=n: it = end(); (it += size()) == begin(): ";
	it = v1->end();
	cout << (((it -= v1->size()) == v1->begin()) ? "True" : "False") << endl;

	// Iterate through the vector []
	cout << endl << "Display all vector Potatoes indexing[] a iterator";
	cout << " (begin(), [])" << endl;
	for (size_t i = 0; i < v1->size(); i++)
		cout << v1->begin()[i] << endl;

	delete v1;
}

/*
 * Battery of tests for vector's reverse iterators.
 */
void tests_vector_reverse_iterator()
{
	NAMESPACE::vector<Potato>* v1;

	cout << endl << "==> Vector reverse_iterator tests" << endl << endl;

	cout << "vector<Potato>(5) (and initialized with ints 0 to 4)" << endl;
	v1 = new NAMESPACE::vector<Potato>(5);
	for (size_t i = 0; i < v1->size(); i++)
		v1->at(i).set_n(i);

	// Testing rev_it+n, ==
	cout << endl << "rev_it+n: (rbegin() + size()) == rend(): ";
	cout << (((v1->rbegin() + v1->size()) == v1->rend()) ? "True" : "False") << endl;
	// Testing n+_rev_it, ==
	cout << "n+rev_it: (size() + rbegin()) == rend(): ";
	cout << ((v1->size() + v1->rbegin()) == v1->rend() ? "True" : "False") << endl;

	// Testing rev_it-n, ==
	cout << endl << "rev_it-n: (rend() - size()) == rbegin(): ";
	cout << ((v1->rend() - v1->size()) == v1->rbegin() ? "True" : "False") << endl;
	// Testing rev_it-rev_it, ==
	cout << "rev_it-rev_it: (rend() - rbegin()) == size(): ";
	cout << ((v1->rend() - v1->rbegin()) ==
			static_cast<rev_iterator_t::difference_type>(v1->size()) ?
			"True" : "False") << endl;

	{
		// Testing rev_it++, ==
		cout << endl << "rev_it++: rev_it(rend()); rev_it++ == rend(): ";
		rev_iterator_t rev_it(v1->rend());
		cout << ((rev_it++ == v1->rend()) ? "True" : "False") << endl;
	}

	{
		// Testing ++rev_it, ==, rev_it+n
		cout << endl << "++rev_it: rev_it(rend()); ++rev_it == (rend() + 1): ";
		rev_iterator_t rev_it(v1->rend());
		cout << ((++rev_it == (v1->rend() + 1)) ? "True" : "False") << endl;
	}

	{
		// Testing rev_it--, ==
		cout << endl << "rev_it--: rev_it(rend()); rev_it-- == rend(): ";
		rev_iterator_t rev_it(v1->rend());
		cout << ((rev_it-- == v1->rend()) ? "True" : "False") << endl;
	}

	{
		// Testing --rev_it, ==, rev_it-n
		cout << endl << "--rev_it: rev_it(rend()); --rev_it == (rend() - 1): ";
		rev_iterator_t rev_it(v1->rend());
		cout << ((--rev_it == (v1->rend() - 1)) ? "True" : "False") << endl;
	}

	// Testing rev_it<rev_it
	cout << endl << "rev_it<rev_it: rbegin() < rend(): ";
	cout << ((v1->rbegin() < v1->rend()) ? "True" : "False") << endl;

	// Testing rev_it>rev_it
	cout << endl << "rev_it>rev_it: rend() > rbegin(): ";
	cout << ((v1->rend() > v1->rbegin()) ? "True" : "False") << endl;

	// Testing rev_it<=rev_it
	cout << endl << "rev_it<=rev_it: rbegin() <= rend(): ";
	cout << ((v1->rbegin() <= v1->rend()) ? "True" : "False") << endl;
	cout << "rev_it<=rev_it: rend() <= rend(): ";
	cout << ((v1->rend() <= v1->rend()) ? "True" : "False") << endl;

	// Testing rev_it>=rev_it
	cout << endl << "rev_it>=rev_it: rend() >= rbegin(): ";
	cout << ((v1->rend() >= v1->rbegin()) ? "True" : "False") << endl;
	cout << "rev_it>=rev_it: rend() >= rend(): ";
	cout << ((v1->rend() >= v1->rend()) ? "True" : "False") << endl;

	{
		// Testing =, +=, ==
		cout << endl << "rev_it+=n: rev_it(rbegin()); (rev_it += size()) == rend(): ";
		rev_iterator_t rev_it(v1->rbegin());
		cout << (((rev_it += v1->size()) == v1->rend()) ? "True" : "False") << endl;
	}

	{
		// Testing =, -=, ==
		cout << endl << "rev_it-=n: rev_it(rend()); (rev_it -= size()) == rbegin(): ";
		rev_iterator_t rev_it(v1->rend());
		cout << (((rev_it -= v1->size()) == v1->rbegin()) ? "True" : "False") << endl;
	}

	// Iterate through the vector []
	cout << endl << "Display all vector Potatoes indexing[] a rev_iterator";
	cout << " (rbegin(), [])" << endl;
	for (size_t i = 0; i < v1->size(); i++)
		cout << v1->rbegin()[i] << endl;

	delete v1;
}
