/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <cctype>

#include "tests.hpp"

using std::cout; using std::endl;
using std::cerr;

struct args
{
	bool vector;
	bool map;
	bool stack;
	unsigned int rnd_seed;
};

/*
 * This function receives the CLI arguments and parses them storing the
 * corresponding flags into the args struct. Returns true on success,
 * and false if it reads an unknown flag.
 */
static bool parse_args(int argc, char *argv[], struct args& args)
{
	args.vector = args.map = args.stack = false;
	args.rnd_seed = time(NULL);

	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "--vector") == 0)
			args.vector = true;
		else if (strcmp(argv[i], "--map") == 0)
			args.map = true;
		else if (strcmp(argv[i], "--stack") == 0)
			args.stack = true;
		else if (strcmp(argv[i], "--rnd-seed") == 0)
		{
			if (i + 1 == argc)
			{
				cerr << "Missing random seed" << endl;
				return (false);
			}
			else if (!isdigit(argv[++i][0]))
			{
				cerr << "Invalid random seed: " << argv[i] << endl;
				return (false);
			}
			args.rnd_seed = atoi(argv[i]);
		}
		else
		{
			cerr << "Invalid argument: " << argv[i] << endl;
			return (false);
		}
	}
	return (true);
}

int main(int argc, char *argv[])
{
	struct args args;

	if (!parse_args(argc, argv, args))
		return (1);

	cout << "======================" << endl;
#if STL == 1
	cout << "Testing STL containers";
#else
	cout << "Testing FT containers";
#endif
	cout << endl << "======================" << endl << endl;

	cout << "Random seed: " << args.rnd_seed << endl << endl;
	srand(args.rnd_seed);

	if (args.vector)
		test_with_time_measure(tests_vector, "vector");
	/*
	 * TODO
	if (args.map)
		test_with_time_measure(tests_map, "map");
	*/
	if (args.stack)
		test_with_time_measure(tests_stack, "stack");

	return (0);
}
