/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/time.h>
#include <iostream>

using std::cout; using std::endl;

/*
 * This function returns the time since the Epoch in microseconds.
 */
static unsigned long get_time()
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return (tv.tv_sec * 1000000 + tv.tv_usec);
}

/*
 * This function executes the given test measuring its time to display it
 * once is completed.
 */
void test_with_time_measure(void (*test)(), const char* container_name)
{
	unsigned long before;
	unsigned long after;
	unsigned long delta;

	before = get_time();
	test();
	after = get_time();

	delta = after - before;
	cout << endl << "==> Execution time of " << container_name << ": ";
	cout << delta / 1000000 << " secs and ";
	cout << delta % 1000000 << " usecs" << endl << endl;
}
