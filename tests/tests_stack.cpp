/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstdlib>
#include "Potato.hpp"
#include "tests.hpp"

#if STL == 1
# include <stack>
# include <vector>
#else
# include "../ft_containers/stack/ft_stack.hpp"
# include "../ft_containers/vector/ft_vector.hpp"
#endif

using std::cout; using std::endl;

/*
 * Passing a vector to a stl stack constructor won't compile since it uses a
 * deque by default (unlike ft::stack that uses ft::vector), and therefore
 * explicitly passing it to the template is required so the code works for
 * both stl and ft stack versions.
 *
 * Another reason to use this typedef instead of just NAMESPACE::stack<Potato>
 * is so the time comparison between ft and stl versions is fair by ensuring
 * they use the same type of inner data structure.
 */
typedef NAMESPACE::stack<Potato, NAMESPACE::vector<Potato> > stack_vector;

/*
 * Battery of tests for the constructor.
 */
static void test_constructor()
{
	NAMESPACE::vector<Potato>* v1;
	int rand_n = rand() % 150 + 1;

	cout << "==> Stack constructor tests" << endl << endl;

	cout << "Size of an default constructed stack: ";
	cout << stack_vector().size() << endl;

	v1 = new NAMESPACE::vector<Potato>(rand_n);
	cout << "Size of a stack constructed from a vector of size " << rand_n;
	cout << ": " << stack_vector(*v1).size() << endl;
	delete v1;
}

/*
 * Battery of tests for the capacity methods.
 */
static void test_capacity_methods()
{
	cout << endl << "==> Stack capacity tests" << endl << endl;

	cout << "empty() on a empty stack: ";
	cout << (stack_vector().empty() ? "True" : "False") << endl;
	cout << "empty() == false on a non-empty stack: ";
	cout << (!stack_vector(NAMESPACE::vector<Potato>(10)).empty() ?
			"True" : "False") << endl;
}

/*
 * Battery of tests for the element access and modifiers methods.
 */
static void test_elem_access_modifiers_methods()
{
	stack_vector* s1;
	int rand_n;

	cout << endl << "==> Stack element access and modifiers tests" << endl;

	for (int trial = 0; trial < 3; trial++)
	{
		s1 = new stack_vector();
		rand_n = rand() % 140 + 10;

		// push()
		cout << endl << "Adding " << rand_n;
		cout << " elements to a new stack using push()" << endl;
		for (int i = 0; i < rand_n; i++)
			s1->push(Potato(i));
		cout << "Size: " << s1->size() << endl;

		// top()
		cout << "Top of the stack using top(): " << s1->top() << endl;

		// pop()
		cout << "Empty the stack calling " << rand_n << " times to pop()" << endl;
		for (int i = 0; i < rand_n; i++)
			s1->pop();
		cout << "Size: " << s1->size() << endl;

		delete s1;
	}
}

/*
 * Battery of tests for the non-member methods.
 */
static void test_non_members()
{
	stack_vector* s1;
	stack_vector* s2;

	cout << endl << "==> Stack non-members tests" << endl << endl;

	s1 = new stack_vector(NAMESPACE::vector<Potato>(150));
	s2 = new stack_vector(NAMESPACE::vector<Potato>(150));

	cout << "Comparing two equal stacks" << endl;
	cout << "stack_1 == stack_2: ";
	cout << ((*s1 == *s2) ? "True" : "False") << endl;
	cout << "(stack_1 != stack_2) == False: ";
	cout << (!(*s1 != *s2) ? "True" : "False") << endl;

	s2->push(Potato(10));
	cout << endl << "Comparing two unequal vectors" << endl;
	cout << "(stack_1 == stack_2) == False: ";
	cout << (!(*s1 == *s2) ? "True" : "False") << endl;
	cout << "stack_1 != stack_2: ";
	cout << ((*s1 != *s2) ? "True" : "False") << endl;

	cout << endl << "Given a bigger stack_2" << endl;
	cout << "stack_1 < stack_2: ";
	cout << ((*s1 < *s2) ? "True" : "False") << endl;
	cout << "(stack_1 > stack_2) == False: ";
	cout << (!(*s1 > *s2) ? "True" : "False") << endl;
	cout << "stack_1 <= stack_2: ";
	cout << ((*s1 <= *s2) ? "True" : "False") << endl;
	cout << "(stack_1 >= stack_2) == False: ";
	cout << (!(*s1 >= *s2) ? "True" : "False") << endl;

	delete s1;
	delete s2;
}

/*
 * Battery of tests for stack.
 */
void tests_stack()
{
#if STL == 1
	cout << "###################" << endl;
	cout << "# STL STACK TESTS #" << endl;
	cout << "###################" << endl << endl;
#else
	cout << "##################" << endl;
	cout << "# FT STACK TESTS #" << endl;
	cout << "##################" << endl << endl;
#endif

	test_constructor();
	test_capacity_methods();
	test_elem_access_modifiers_methods();
	test_non_members();
}
